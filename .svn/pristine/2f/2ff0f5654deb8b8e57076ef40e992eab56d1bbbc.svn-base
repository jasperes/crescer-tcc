package cwi.comerciator.controller;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cwi.comerciator.dao.ProdutoDao;
import cwi.comerciator.model.Produto;

@Controller
public class ProdutoController {
	
	@Inject
	private ProdutoDao produtoDao;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String abreIndex(Model model){
		return "index.html";
	}
	
	@ResponseBody
	@RequestMapping(value="/produtos/buscaDescontos", method = RequestMethod.GET)
		public List<Produto> buscaUltimosProdutos(Model model){
			return produtoDao.buscaProdutosComMaioresDescontos();
	}
	
	@RequestMapping(value = "/produtos", method = RequestMethod.GET)
	public String abreProdutos(Model model){
		return "produtos.html";
	}
	
	@ResponseBody
	@RequestMapping(value="/produtos/buscaTodosProdutos", method = RequestMethod.GET)
		public List<Produto> buscaTodosProdutos(Model model){
		return produtoDao.selecionarTodos();
	}
	
	@RequestMapping(value="/produtos/pesquisa", method = RequestMethod.GET)
		public String abrePesquisas(@RequestParam("busca") String busca, Model model ){
			List<Produto> produtos = produtoDao.buscaProdutosPorNomeOuDescricao(busca);
			model.addAttribute("produtos", produtos);
			if(produtos.size() < 1){
				model.addAttribute("mensagem", "Nenhum produto encontrado!");
			}else{
				model.addAttribute("mensagem", "");
			}
			return "produtos-pesquisa.html";
		}
	@RequestMapping(value = "/produtos/detalhes", method = RequestMethod.GET)
	public String abreDetalhesProdutos(@RequestParam("id") int id, Model model){
		return "detalhes-produtos.html";
	}
	
	@ResponseBody
	@RequestMapping(value="/detalhes", method = RequestMethod.GET)
		public Produto buscaDetalhesProduto(@RequestParam("id") int id, Model model){
		Produto produto = produtoDao.buscaProdutoPorId(id).get(0);
		return produto;
	}
	@ResponseBody
	@RequestMapping(value="/produtos/buscaSimilares", method = RequestMethod.GET)
	public List<Produto> buscaSimilares(@RequestParam("filtro") String filtro, Model model){
		return produtoDao.buscaProdutosPorNomeOuDescricao(filtro);
	}
}
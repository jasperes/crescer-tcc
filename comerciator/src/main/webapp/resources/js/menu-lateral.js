$(document).ready(function(){
	$.ajax({
		url:"/tag/itensMenu",
		type:"GET",
		dataType:"json",
		success:function(resposta){
			$.each(resposta, function(i,descricoes){
				var classificacao = i;
				var menuClassificacao = '<div class="panel panel-default"><div class="panel-heading"><h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordian" href="#'+classificacao+'"><span class="badge pull-right"><i class="fa fa-plus"></i></span>'+classificacao+'</a></h4></div><div id="'+classificacao+'" class="panel-collapse collapse"><div class="panel-body"><ul id="ul-'+classificacao+'"></ul></div></div></div>';

				$("#accordian").append(menuClassificacao);
				$.each(descricoes, function(j,descricao){
					$("#ul-"+classificacao).append(
						$("<li>").append(
							$("<a href='/produtos/pesquisaTag?busca="+descricao+"&pagina=1'>").append(descricao)
						)
					);

				})
			})
		}
	})
});
$(document).ready(function(){
	$.ajax({
		url:'/produtos/buscaTodosProdutos',
		type: 'GET', 
		dataType: 'json',
		success: function(resposta){
			
			console.log(resposta);
			for(var i = 0;i<resposta.length;i++){	 				
			var descricao = resposta[i].descricao;
			var imagem = resposta[i].imagem;
			var nome = resposta[i].nome;
			var preco = resposta[i].preco;
			var desconto = resposta[i].desconto;
			var precoDesconto = preco - ((preco * desconto)/100);
			
			var html='<div class="col-sm-4"><div class="product-image-wrapper"><div class="single-products"><div class="productinfo text-center"><img height=250px width=200px src='+imagem+' alt="produto" />'+(desconto > 0 ?'<h5><s>'+toMoney(preco)+'</s></h5>' : '')+'<h2>'+toMoney(precoDesconto)+'</h2><p>'+nome+'</p><a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Adicionar ao carrinho</a></div></div></div></div>';
			$("#produtos-loja").append(html);
				
			
			}
		}
	})
});
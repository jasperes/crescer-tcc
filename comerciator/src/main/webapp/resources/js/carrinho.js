$(function(){
	
	// Cria item da lista de compras do carrinho.
	function adicionaItemListaCarrinho(itemCarrinho){
		
		var produto = itemCarrinho["produto"];
		
		var idProduto = produto["idProduto"];
		var imagem = produto["imagem"];
		var descricao = produto["nome"];
		var preco = produto["preco"] - ((produto["preco"] / 100 ) * produto["desconto"]);
		var quantidade = itemCarrinho["quantidade"];
		var precoTotal = preco * quantidade;
		
		somaNoTotal(precoTotal);
		
		$("#tbody-carrinho").append(
			$("<tr id='produto-"+idProduto+"'>").append(
				// imagem do produto
				$("<td class='cart_product'>").append(
					$("<a href=''>").append(
						$("<img src='"+imagem+"' alt='imagem do produto'>").css(
							{"width":"110px","height":"110px"}
						)
					)
				),
				// descrição do produto
				$("<td class='cart_description'>").append(
					$("<a href=''>").append(
						$("<p>").append(descricao)
					)
				),
				// preço com desconto do produto
				$("<td class='cart_price'>").append(
					$("<p>").append(toMoney(preco))
				),
				// quantidade à comprar do produto
				$("<td class='cart_quantity'>").append(
					$("<div class='cart_quantity_button'>").append(
						// aumenta quantidade
						$("<a class='cart_quantity_up' href='#' id='cart-up-"+idProduto+"'>").append("+"),
						// quantidade atual
						$("<input id='quantidade-produto-"+idProduto+"' class='cart_quantity_input' type='text' name='quantity' " +
							"value='"+quantidade+"' autocomplete='off' size='2'>"),
						// diminui quantidade
						$("<a class='cart_quantity_down' href='#' id='cart-down-"+idProduto+"'>").append("-")
					)
				),
				// preço total, somando = (quantidade * preço com desconto)
				$("<td class='cart_total'>").append(
					$("<p class='cart_total_price' id='preco-total-"+idProduto+"'>").append(toMoney(precoTotal))
				),
				// botão para deletar o produto da lista de compras do carrinho
				$("<td class='cart_delete'>").append(
					$("<a class='cart_quantity_delete' href='#' id='cart-delete-"+idProduto+"'>").append(
						$("<i class='fa fa-times'>")
					)
				)
			)
		);
		
		// Funções:
		// Ao adicionar +1 em um produto
		$("#cart-up-"+idProduto).on("click", function(){
			$.post("/carrinho/adiciona", produto)
				.done(function(){
					var novoValor = parseInt($("#quantidade-produto-"+idProduto).attr("value")) + 1;
					$("#quantidade-produto-"+idProduto).attr("value", novoValor);
					precoTotal = preco * novoValor;
					somaNoTotal(preco);
					$("#preco-total-"+idProduto).empty().append(toMoney(precoTotal));
			});
			return false;
		});
		
		// Ao alterar quantidade do produto
		$("#quantidade-produto-"+idProduto).change(function(){
			var novaQuantidade = parseInt($("#quantidade-produto-"+idProduto).val());
			
			compraCarrinho = {};
			compraCarrinho.produto = produto;
			compraCarrinho.quantidade = novaQuantidade;
			
			$.post("/carrinho/atualiza", compraCarrinho)
				.done(function(){
					somaNoTotal(precoTotal * -1);
					precoTotal = preco * novaQuantidade;
					somaNoTotal(precoTotal);
					$("#preco-total-"+idProduto).empty().append(toMoney(precoTotal));
				})
		});
		
		// Ao remover -1 em um produto
		$("#cart-down-"+idProduto).on("click", function(){
			if(parseInt($("#quantidade-produto-"+idProduto).attr("value")) < 2){return false;};
			$.post("/carrinho/remove", produto)
				.done(function(){
					var novoValor = parseInt($("#quantidade-produto-"+idProduto).attr("value")) - 1;
					$("#quantidade-produto-"+idProduto).attr("value", novoValor);
					precoTotal = preco * novoValor;
					somaNoTotal(preco * -1);
					$("#preco-total-"+idProduto).empty().append(toMoney(precoTotal));
				});
			return false;
		});
		
		// Ao excluir um produto
		function excluiProduto(){
			somaNoTotal(parseInt($("#preco-total-"+idProduto).html().split(" ")[1]) * -1);
			$("#produto-"+idProduto).remove();
		};
		// Quando clicar em excluir
		$("#cart-delete-"+idProduto).on("click", function(){
			$.post("/carrinho/exclui", produto)
				.done(function(){
					// Se não tiver mais produtos na lista, exibe mensagem.
					// Se tiver apenas remove o produto excluido da tabela.
					$("#tbody-carrinho tr").length === 1 ? 
						adicionaMensagemSemProdutosCarrinho() : excluiProduto();
				});
			return false;
		});
	};
	
	// Adicionar uma mensagem caso não tenha produtos no carrinho
	function adicionaMensagemSemProdutosCarrinho() {
		$("#carrinho-container").empty().removeAttr("class").append(
			$("<h1>").append(
				$("<i>").append(
					"Não há produtos adicionados ao carrinho,",
					$("<br>"),
					"clique ",
					$("<a href='/'>").append("aqui"),
					" para começar a adicionar!"
				)
			)
		).css({"text-align":"center"});
		$("#carrinho-buttons").empty();
		$("#carrinho-container").removeAttr("hidden");
	};
	
	// Calcula valor total à pagar e exibe no rodapé do conteúdo.
	var valorTotal = 0;
	function somaNoTotal(valor) {
		valorTotal += valor;
		$("#span-preco-total").empty().append(toMoney(valorTotal));
	};
	
	// pega lista de carrinho.
	function getListaCarrinho(){
		$.get("/carrinho/lista", function(carrinho){
			// Se não tiver nada na lista, exibe mensagem
			if(carrinho.length === 0){
				adicionaMensagemSemProdutosCarrinho();
			// Se tiver algo na lista, exibe
			}else{
				$("#carrinho-container").removeAttr("hidden");
				$("#carrinho-buttons").removeAttr("hidden");
				$.each(carrinho, function(i,item){
					adicionaItemListaCarrinho(item);
				});
			};
		});
	};
	
	// executa a função para pegar lista do carrinho
	// ao carregar a página.
	getListaCarrinho();
	
});
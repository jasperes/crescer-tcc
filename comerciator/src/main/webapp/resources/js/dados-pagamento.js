$(function(){
	
	// Cria item da lista de compras do carrinho.
	function adicionaItemListaCarrinho(itemCarrinho){
		
		var produto = itemCarrinho["produto"];
		
		var idProduto = produto["idProduto"];
		var imagem = produto["imagem"];
		var descricao = produto["nome"];
		var preco = produto["preco"] - ((produto["preco"] / 100 ) * produto["desconto"]);
		var quantidade = itemCarrinho["quantidade"];
		var precoTotal = preco * quantidade;
		
		somaNoTotal(precoTotal);
		
		$("#tbody-carrinho").append(
			$("<tr id='produto-"+idProduto+"'>").append(
				// imagem do produto
				$("<td class='cart_product'>").append(
					$("<a href=''>").append(
						$("<img src='"+imagem+"' alt='imagem do produto'>").css(
							{"width":"110px","height":"110px"}
						)
					)
				),
				// descrição do produto
				$("<td class='cart_description'>").append(
					$("<a href=''>").append(
						$("<p>").append(descricao)
					)
				),
				// preço com desconto do produto
				$("<td class='cart_price'>").append(
					$("<p>").append(toMoney(preco))
				),
				// quantidade à comprar do produto
				$("<td class='cart_quantity'>").append(
					$("<div class='cart_quantity_button'>").append(
						// quantidade atual
						$("<span>").append(quantidade)
					)
				),
				// preço total, somando = (quantidade * preço com desconto)
				$("<td class='cart_total'>").append(
					$("<p class='cart_total_price' id='preco-total-"+idProduto+"'>").append(toMoney(precoTotal))
				)
			)
		);
	};
		
	// calcula valor total da compra
	var valorTotal = 0;
	function somaNoTotal(valor) {
		valorTotal += valor;
	};
	
	// pega lista de carrinho.
	function getListaCarrinho(){
		$.get("/carrinho/lista", function(carrinho){
			$("#carrinho-container").removeAttr("hidden");
			$("#carrinho-buttons").removeAttr("hidden");
			$.each(carrinho, function(i,item){
				adicionaItemListaCarrinho(item);
			});
			
			$("#span-preco-total").append(toMoney(valorTotal));
			
		});
	};
	
	function mascaraCampos() {
		$("#numero").mask("9999 9999 9999 9999");
		$("#validade").mask("99/99");
		$("#codigo").mask("999");
		
		$("#cep").mask("99999-999");
		$("#uf").mask("**");
	}
	
	/* Ao Finalizar Compra */
	$("#finaliza-compra").on("click", function(){
		var compra = {};
		compra.endereco = $("#endereco").val();
		compra.complemento = $("#complemento").val();
		compra.cep = $("#cep").val();
		compra.cidade = $("#cidade").val();
		compra.uf = $("#uf").val();
		compra.pais = $("#pais").val();
		compra.valor = toMoney(valorTotal);
		
		$.post("/pagamento/dados/valida", compra)
		location.href = "/pagamento/sucesso";
	})

	// executa a função para pegar lista do carrinho
	// ao carregar a página.
	getListaCarrinho();
	mascaraCampos();
});
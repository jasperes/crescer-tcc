package cwi.comerciator.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import cwi.comerciator.model.Destaque;

@Component
public class DestaqueDao {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	/**
	 * Salva o Destaque.
	 * 
	 * Caso o cadastro não exista, cria um.
	 * Caso o cadastro já exista, atualiza os dados.
	 * 
	 * Verifica o cadastro pelo ID.
	 * 
	 * @param Destaque Destaque à gravar.
	 * @return Destaque Destaque criado ou atualizado.
	 */
	@Transactional
	public Destaque salvar(Destaque destaque){
		return entityManager.merge(destaque);
	}
	
	/**
	 * Seleciona todos os Destaques.
	 * @return List<Destaque> Lista de Destaque.
	 */
	@Transactional
	public List<Destaque> selecionarTodos() {
		String sql = "SELECT i FROM Destaque i";
		TypedQuery<Destaque> query = entityManager.createQuery(sql, Destaque.class);
		return query.getResultList();
	}

}

package cwi.comerciator.dao;

import java.util.List;
import java.util.Random;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import cwi.comerciator.model.Produto;
import cwi.comerciator.model.Tag;

@Component
public class ProdutoDao {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Transactional
	public Produto buscaProdutoPorId(Integer idProduto){
		String sql = "SELECT i FROM Produto i where i.idProduto = :idProduto";
		TypedQuery<Produto> query = entityManager.createQuery(sql, Produto.class);
		query.setParameter("idProduto", idProduto);
		return query.getSingleResult();
	}
	
	@Transactional
	public List<Produto> buscaUltimosProdutos() {
		TypedQuery<Produto> query = entityManager.createQuery("SELECT i FROM Produto i ORDER BY i.idProduto DESC ", Produto.class);
		query.setMaxResults(6);
		return query.getResultList();
	}
	
	@Transactional
	public List<Produto> buscaProdutosComMaioresDescontos(){
		TypedQuery<Produto> query=entityManager.createQuery("SELECT i FROM Produto i ORDER BY i.desconto DESC", Produto.class);
		query.setMaxResults(6);
		return query.getResultList();
	}	
	@Transactional
	public List<Produto> buscaProdutosPorNomeOuDescricao(String busca, Integer pagina){
		TypedQuery<Produto> query=entityManager.createQuery("SELECT i FROM Produto i where upper(i.nome) like UPPER(:nome) or upper(i.descricao) like upper(:descricao)", Produto.class);
		query.setParameter("nome","%"+ busca+"%");
		query.setParameter("descricao", "%"+ busca+"%");
		query.setFirstResult((pagina-1)*9);
	    query.setMaxResults(9);
		return query.getResultList();
	}
	

	@Transactional
	public Long getTotalProdutos(String busca){
		TypedQuery<Long> query=entityManager.createQuery("SELECT COUNT(i) FROM Produto i where upper(i.nome) like UPPER(:nome) or upper(i.descricao) like upper(:descricao)", Long.class);
		query.setParameter("nome","%"+ busca+"%");
		query.setParameter("descricao", "%"+ busca+"%");
		return query.getSingleResult();
	}

	@Transactional
	public List<Produto> buscaPorTag(String tag, Integer pagina) {
		String sql = "select p from TagProduto tp"
				+ " join tp.produto p"
				+ " join tp.tag t"
				+ " where t.descricao = :tag";
	
		TypedQuery<Produto> query = entityManager.createQuery(sql, Produto.class);
		query.setParameter("tag", tag);
		query.setFirstResult((pagina-1)*9);
	    query.setMaxResults(9);
		
		return query.getResultList();
	}
	
	@Transactional
	public Long getTotalProdutosPorTag(String busca){
		String sql = "select count(p) from TagProduto tp"
				+ " join tp.produto p"
				+ " join tp.tag t"
				+ " where t.descricao = :tag";

		TypedQuery<Long> query = entityManager.createQuery(sql, Long.class);
		query.setParameter("tag", busca);
		return query.getSingleResult();
	}
	
	@Transactional
	public List<Tag> tagsDoProduto(Integer idProduto) {
		String sql1 = "select t from TagProduto tp"
				+ " join tp.tag t"
				+ " join tp.produto p "
				+ " where p.idProduto = :idProduto";
	
		TypedQuery<Tag> query1 = entityManager.createQuery(sql1, Tag.class);
		query1.setParameter("idProduto", idProduto);
		return query1.getResultList();
	}
	
	@Transactional
	public List<Produto> buscaProdutosRelacionados(Integer idProduto) {
		
		List<Tag> tags = this.tagsDoProduto(idProduto);
		int tamanho = tags.size();
		Random random = new Random();
		Tag tag = tags.get(random.nextInt(tamanho));
		
		String sql2 = "select p from TagProduto tp"
					+ " join tp.produto p"
					+ " join tp.tag t"
					+ " where t.descricao = :tag"
					+ " and p.idProduto != :idProduto";
		
		TypedQuery<Produto> query2 = entityManager.createQuery(sql2, Produto.class);
		query2.setParameter("tag", tag.getDescricao());
		query2.setParameter("idProduto", idProduto);
		query2.setMaxResults(3);
		
		return query2.getResultList();
	}


}

package cwi.comerciator.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import cwi.comerciator.model.Tag;
@Component
public class TagDao {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Transactional
	public List<Tag> selecionaTodasTags(){
		TypedQuery<Tag> query=entityManager.createQuery("SELECT t FROM Tag t", Tag.class);
		return query.getResultList();
	}
}

package cwi.comerciator.dao;

import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import cwi.comerciator.model.Compra;

@Component
public class CompraDao {

	@PersistenceContext
	private EntityManager entityManager;
	
	/**
	 * Salva a Compra.
	 * 
	 * Caso o cadastro não exista, cria um.
	 * Caso o cadastro já exista, atualiza os dados.
	 * 
	 * Verifica o cadastro pelo ID.
	 * 
	 * @param Compra Compra à gravar.
	 * @return Compra Compra criado ou atualizado.
	 */
	@Transactional
	public Compra salvar(Compra compra) {
		String codigo = UUID.randomUUID().toString();
		compra.setCodigo(codigo);
		return entityManager.merge(compra);
	}
	
}

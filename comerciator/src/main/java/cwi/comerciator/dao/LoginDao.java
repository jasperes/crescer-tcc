package cwi.comerciator.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import cwi.comerciator.model.Login;

@Component
public class LoginDao {

	@PersistenceContext
	private EntityManager entityManager;
	
	/**
	 * Salva o login do usuário.
	 * 
	 * Caso o cadastro não exista, cria um.
	 * Caso o cadastro já exista, atualiza os dados.
	 * 
	 * Verifica o cadastro pelo ID.
	 * 
	 * @param Login Login à gravar.
	 * @return Login Login criado ou atualizado.
	 */
	@Transactional
	public Login salvar(Login login){
		return entityManager.merge(login);
	}
	
	/**
	 * Verifica se o email já existe.
	 * @param login Cadastro à verificar.
	 * @return boolean Verdade caso exista, falso do contrário.
	 */
	@Transactional
	public boolean emailExiste(Login login) {
		StringBuilder builder = new StringBuilder();
		
		builder.append("SELECT COUNT(l) FROM Login l");
		builder.append(" WHERE email = :email");
		
		String sql = builder.toString();
		
		TypedQuery<Long> query = entityManager.createQuery(sql, Long.class);
		query.setParameter("email", login.getEmail());
		
		boolean emailExiste = query.getSingleResult() > 0 ? true : false;
		
		return emailExiste;
	}
	
	/**
	 * Valida os dados de login, se o usuario e senha estão corretos.
	 * @param login Dados à validar.
	 * @return boolean Verdadeiro caso os dados estejam corretos, falso do contrário.
	 */
	@Transactional
	public boolean validaDados(Login login) {
		StringBuilder builder = new StringBuilder();
		
		builder.append("SELECT COUNT(l) FROM Login l");
		builder.append(" WHERE UPPER(email) = UPPER(:email)");
		builder.append(" AND senha = :senha");
		
		String sql = builder.toString();
		
		TypedQuery<Long> query =  entityManager.createQuery(sql, Long.class);
		query.setParameter("email", login.getEmail());
		query.setParameter("senha", login.getSenha());
		
		boolean dadosCorretos = query.getSingleResult() == 1 ? true : false;
		
		return dadosCorretos;
	}
	
	/**
	 * Obtem objeto Login através dos dados Email e Senha.
	 * @param login Objeto Login à pesquisar.
	 * @return Login Objeto Login encontrado.
	 */
	public Login buscaLoginPorDados(Login login) {
		StringBuilder builder = new StringBuilder();
		
		builder.append("SELECT l FROM Login l");
		builder.append(" WHERE UPPER(email) = UPPER(:email)");
		builder.append(" AND senha = :senha");
		
		String sql = builder.toString();
		
		TypedQuery<Login> query =  entityManager.createQuery(sql, Login.class);
		query.setParameter("email", login.getEmail());
		query.setParameter("senha", login.getSenha());
		
		return query.getSingleResult();
	}
	
	
}

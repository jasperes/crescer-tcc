package cwi.comerciator.model;

public class Paginacao {

	private Integer numeroPagina;
	private boolean paginaAtual;
	
	public Paginacao(int numeroPagina, boolean paginaAtual){
		this.numeroPagina = numeroPagina;
		this.paginaAtual = paginaAtual;
	}
	
	public Integer getNumeroPagina() {
		return numeroPagina;
	}
	
	public boolean isPaginaAtual() {
		return paginaAtual;
	}
	
	public void setNumeroPagina(Integer numeroPagina) {
		this.numeroPagina = numeroPagina;
	}
	
	public void setPaginaAtual(boolean paginaAtual) {
		this.paginaAtual = paginaAtual;
	}
	
}

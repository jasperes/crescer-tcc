package cwi.comerciator.model;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.core.io.ClassPathResource;

import com.google.common.base.Charsets;
import com.google.common.io.Files;

public class RealizaCompra extends Thread{
	
	private Compra compra;
	private Login login;
	
	public RealizaCompra(Compra compra, Login login) {
		this.compra = compra;
		this.login = login;
	}

	public void run(){
		
		try {
			this.enviaEmail();
		} catch (AddressException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private void enviaEmail() throws AddressException, MessagingException, IOException {
		final String username = "comerciator.estonia@gmail.com";
		final String password = "comerciator123";
 
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
 
		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		  });
			
		ClassPathResource resource = new ClassPathResource("email.html");
		String texto = Files.toString(resource.getFile(), Charsets.UTF_8);
		
		texto = texto.replace("--USUARIO", login.getNome().split(" ")[0]);
		texto = texto.replace("--CODIGO", compra.getCodigo());
		texto = texto.replace("--ENDERECO", compra.getEndereco());
		texto = texto.replace("--COMPLEMENTO", compra.getComplemento());
		texto = texto.replace("--CEP", compra.getCep());
		texto = texto.replace("--CIDADE", compra.getCidade());
		texto = texto.replace("--UF", compra.getUf());
		texto = texto.replace("--PAIS", compra.getPais());
		texto = texto.replace("--PRECO", compra.getValor());
 
		Message message = new MimeMessage(session);
		message.setFrom(new InternetAddress("jaspjonathan@gmail.com"));
		message.setRecipients(Message.RecipientType.TO,
			InternetAddress.parse(login.getEmail()));
		message.setSubject("Confirmação de sua compra");
		message.setContent(texto, "text/html; charset=utf-8");
 
		Transport.send(message);
	}
}

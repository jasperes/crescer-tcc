package cwi.comerciator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name = "DESTAQUE_SEQ", sequenceName = "DESTAQUE_SEQ")
public class Destaque {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DESTAQUE_SEQ")
	private Integer idDestaque;
	
	@Column(nullable = false)
	private String titulo;
	
	@Column(nullable = false)
	private String conteudo;
	
	@Column(nullable = false)
	private String link;
	
	@Column(nullable = false)
	private String imagem;
	
	// Methods Set
	public void setIdDestaque(Integer idDestaque) {
		this.idDestaque = idDestaque;
	}
	
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}
	
	public void setLink(String link) {
		this.link = link;
	}
	
	public void setImagem(String imagem) {
		this.imagem = imagem;
	}
	
	// Methods Get
	public Integer getIdDestaque() {
		return idDestaque;
	}
	
	public String getTitulo() {
		return titulo;
	}

	public String getConteudo() {
		return conteudo;
	}
	
	public String getLink() {
		return link;
	}
	
	public String getImagem() {
		return imagem;
	}
}

package cwi.comerciator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name="TAG_SEQ", sequenceName="TAG_SEQ")
public class Tag {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="TAG_SEQ")
	private Integer idTag;
	
	@Column(nullable = false)
	private String descricao;
	
	@Column(nullable = false)
	private String classificacao;
	
	// Methods Set
	public void setIdTag(Integer idTag) {
		this.idTag = idTag;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public void setClassificacao(String classificacao) {
		this.classificacao = classificacao;
	}
	
	// Methods Get
	public String getDescricao() {
		return descricao;
	}
	
	public Integer getIdTag() {
		return idTag;
	}
	
	public String getClassificacao() {
		return classificacao;
	}
	
}

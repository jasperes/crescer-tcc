package cwi.comerciator.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name = "TAGPRODUTO_SEQ", sequenceName = "TAGPRODUTO_SEQ")
public class TagProduto {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TAGPRODUTO_SEQ")
	private Integer idTagProduto;
	
	@ManyToOne
	@JoinColumn(name = "idTag", nullable = false)
	private Tag tag;
	
	@ManyToOne
	@JoinColumn(name = "idProduto", nullable = false)
	private Produto produto;

	public Integer getIdTagProduto() {
		return idTagProduto;
	}

	public void setIdTagProduto(Integer idTagProduto) {
		this.idTagProduto = idTagProduto;
	}

	public Tag getTag() {
		return tag;
	}

	public void setTag(Tag tag) {
		this.tag = tag;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	
	
	
}

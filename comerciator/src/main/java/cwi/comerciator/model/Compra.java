package cwi.comerciator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name = "COMPRA_SEQ", sequenceName = "COMPRA_SEQ")
public class Compra {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COMPRA_SEQ")
	private Integer idCompra;
	
	@Column(nullable = false)
	private String codigo;
	
	@JoinColumn(name = "idLogin", nullable = false)
	private Integer idLogin;
	
	@Column(nullable = false)
	private String endereco;
	
	@Column(nullable = true)
	private String complemento;

	@Column(nullable = false)
	private String cep;
	
	@Column(nullable = false)
	private String cidade;
	
	@Column(nullable = false)
	private String uf;
	
	@Column(nullable = false)
	private String pais;
	
	@Column(nullable = false)
	private String valor;

	// Methods Set
	public void setIdCompra(Integer idCompra) {
		this.idCompra = idCompra;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public void setIdLogin(Integer idLogin) {
		this.idLogin = idLogin;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	// Methods Get
	public Integer getIdCompra() {
		return idCompra;
	}

	public String getCodigo() {
		return codigo;
	}

	public Integer getIdLogin() {
		return idLogin;
	}

	public String getEndereco() {
		return endereco;
	}
	
	public String getComplemento() {
		return complemento;
	}

	public String getCep() {
		return cep;
	}

	public String getCidade() {
		return cidade;
	}

	public String getUf() {
		return uf;
	}

	public String getPais() {
		return pais;
	}

	public String getValor() {
		return valor;
	}

}

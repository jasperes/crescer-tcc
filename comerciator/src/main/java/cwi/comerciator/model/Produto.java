package cwi.comerciator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name = "PRODUTO_SEQ", sequenceName = "PRODUTO_SEQ")
public class Produto {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PRODUTO_SEQ")
	private Integer idProduto;
	
	@Column(nullable = false)
	private String sku;
	
	@Column(nullable = false)
	private String nome;
	
	@Column(nullable = false)
	private double preco;
	
	@Column(nullable = false)
	private double desconto;
	
	@Column(nullable = false)
	private String descricao;
	
	@Column(nullable = false)
	private String imagem;
	
	// Methods Set
	public void setIdProduto(Integer idProduto) {
		this.idProduto = idProduto;
	}
	
	public void setSku(String sku) {
		this.sku = sku;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setPreco(double preco) {
		this.preco = preco;
	}
	
	public void setDesconto(double desconto) {
		this.desconto = desconto;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public void setImagem(String imagem) {
		this.imagem = imagem;
	}
	
	// Methods Get
	public Integer getIdProduto() {
		return idProduto;
	}
	
	public String getSku() {
		return sku;
	}
	
	public String getNome() {
		return nome;
	}
	
	public double getPreco() {
		return preco;
	}
	
	public double getDesconto() {
		return desconto;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public String getImagem() {
		return imagem;
	}
}

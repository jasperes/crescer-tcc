package cwi.comerciator.controller;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cwi.comerciator.dao.InstrutorDao;

@Controller
public class InstrutorController {

	@Inject
	private InstrutorDao instrutorDao;
	
	@RequestMapping(value = "/intrutores", method = RequestMethod.GET)
	public String abreIndex(Model model){
		model.addAttribute("mensagem", "Iniciando o TCC");
		model.addAttribute("instrutores", instrutorDao.buscarTodos());
		return "instrutores.html";
	}
	
}

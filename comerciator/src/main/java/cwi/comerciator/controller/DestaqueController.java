package cwi.comerciator.controller;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cwi.comerciator.dao.DestaqueDao;
import cwi.comerciator.model.Destaque;

@Controller
public class DestaqueController {
	
	@Inject
	private DestaqueDao destaqueDao;
	
	@ResponseBody
	@RequestMapping(value="/destaques/buscaTodosDestaques", method = RequestMethod.GET)
		public List<Destaque> buscaDestaque(Model model){
			return destaqueDao.selecionarTodos();
	}

}

package cwi.comerciator.controller;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cwi.comerciator.model.Compra;
import cwi.comerciator.model.Login;
import cwi.comerciator.model.RealizaCompra;

import cwi.comerciator.dao.CompraDao;

@Controller
public class PagamentoController {
	
	@Inject
	private CompraDao compraDao;

	@RequestMapping(value = "/pagamento", method = RequestMethod.GET)
	public String pagamento(HttpSession session) {
		session.setAttribute("compraFinalizada", "1");
		if(session.getAttribute("login") == null) {
			return "redirect:/login";
		}else{
			return "redirect:/pagamento/dados";
		}
	}
	
	@RequestMapping(value = "/pagamento/dados", method = RequestMethod.GET)
	public String dadosPagamento(HttpSession session) {
		if(session.getAttribute("login") == null) {
			return "redirect:/login";
		}else if(session.getAttribute("carrinho") == null){
			return "redirect:/carrinho";
		}else{
			return "dados-pagamento.html";
		}
	}
	
	@RequestMapping(value = "/pagamento/dados/valida", method = RequestMethod.POST)
	public String validaDadosPagamento(Compra compra, HttpSession session) {
		/* Não valida nada */
		
		Login login = (Login) session.getAttribute("login");
		
		compra.setIdLogin(login.getIdLogin());
		compraDao.salvar(compra);
		
		new RealizaCompra(compra, login).start();
		
		session.setAttribute("carrinho", null);
		
		return "redirect:/pagamento/sucesso";
	}
	
	@RequestMapping(value = "/pagamento/sucesso", method = RequestMethod.GET)
	public String pagamentoConcluido() {
		return "pagamento-concluido.html";
	}
	
}

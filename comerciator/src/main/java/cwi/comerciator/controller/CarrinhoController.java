package cwi.comerciator.controller;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cwi.comerciator.dao.ProdutoDao;
import cwi.comerciator.model.CarrinhoCompra;
import cwi.comerciator.model.Produto;

@Controller
public class CarrinhoController {
	
	@Inject
	ProdutoDao produtoDao = new ProdutoDao();
	
	@RequestMapping(value = "/carrinho", method = RequestMethod.GET)
	public String carrinho(Model model) {
		return "carrinho.html";
	}

	@RequestMapping(value = "/carrinho/adiciona", method = RequestMethod.POST)
	public String adicionaProdutoCarrinho(Produto produto, HttpSession session) {
		
		// Cria carrinho caso não exista na sessão.
		if(session.getAttribute("carrinho") == null) {
			session.setAttribute("carrinho", new ArrayList<CarrinhoCompra>());
		}
		
		// Pega carrinho da sessão e salva em variavel.
		@SuppressWarnings("unchecked")
		ArrayList<CarrinhoCompra> carrinho = (ArrayList<CarrinhoCompra>) session.getAttribute("carrinho");
		
		// Se o carrinho já existe, appenda +1 na quantidade.	
		// Primeiro, cria variável para verificação se existe;
		boolean carrinhoExiste = false;
		// Para cada CarrinhoCompra cc do carrinho da sessão:
		for(CarrinhoCompra cc : carrinho){
			// Se o SKU do produto é igual ao SKU  do produto do CarrinhoCompra cc
			if(cc.getProduto().getSku().equals(produto.getSku())){
				// Salva quantidade +1 e o index do CarrinhoCompra da lista carrinho
				Integer novaQuantidade =  cc.getQuantidade() + 1;
				Integer index = carrinho.indexOf(cc);
				
				// Aumenta a quantidade do produto em +1 na lista carrinho
				carrinho.get(index).setQuantidade(novaQuantidade);
				
				// Diz que o carrinho já existe e para o loop
				carrinhoExiste = true;
				break;
			}
		}
		
		// Caso o carrinho não exista
		if(!carrinhoExiste){
			// Cria o carrinho e adiciona na lista carrinho
			CarrinhoCompra carrinhoCompra = new CarrinhoCompra(produto, 1);
			carrinho.add(carrinhoCompra);			
		}
		
		// Seta o atributo carrinho com a nova lista
		session.setAttribute("carrinho", carrinho);
		
		// redireciona para a página de carrinho
		return "redirect:/carrinho";
	}
	
	@ResponseBody
	@RequestMapping(value = "/carrinho/atualiza", method = RequestMethod.POST)
	public String remove(CarrinhoCompra carrinhoCompra, HttpSession session) {
		// Pega carrinho da sessão e salva em variavel.
		@SuppressWarnings("unchecked")
		ArrayList<CarrinhoCompra> carrinho = (ArrayList<CarrinhoCompra>) session.getAttribute("carrinho");
		
		// Para cada CarrinhoCompra cc do carrinho da sessão:
		for(CarrinhoCompra cc : carrinho){
			// Se o SKU do produto é igual ao SKU  do produto do CarrinhoCompra cc
			if(cc.getProduto().getSku().equals(carrinhoCompra.getProduto().getSku())){
				// Salva quantidade -1 e o index do CarrinhoCompra da lista carrinho
				Integer novaQuantidade =  carrinhoCompra.getQuantidade();
				Integer index = carrinho.indexOf(cc);
				
				// Aumenta a quantidade do produto em -1 na lista carrinho
				carrinho.get(index).setQuantidade(novaQuantidade);
				
				// Seta o atributo carrinho com a nova lista
				session.setAttribute("carrinho", carrinho);
				
				// para verificação do produto
				break;
			}
		}
		return "";
	}
	
	@ResponseBody
	@RequestMapping(value = "/carrinho/remove", method = RequestMethod.POST)
	public String remove(Produto produto, HttpSession session) {
		// Pega carrinho da sessão e salva em variavel.
		@SuppressWarnings("unchecked")
		ArrayList<CarrinhoCompra> carrinho = (ArrayList<CarrinhoCompra>) session.getAttribute("carrinho");
		
		// Para cada CarrinhoCompra cc do carrinho da sessão:
		for(CarrinhoCompra cc : carrinho){
			// Se o SKU do produto é igual ao SKU  do produto do CarrinhoCompra cc
			if(cc.getProduto().getSku().equals(produto.getSku())){
				// Salva quantidade -1 e o index do CarrinhoCompra da lista carrinho
				Integer novaQuantidade =  cc.getQuantidade() - 1;
				Integer index = carrinho.indexOf(cc);
				
				// Aumenta a quantidade do produto em -1 na lista carrinho
				carrinho.get(index).setQuantidade(novaQuantidade);
				
				// Seta o atributo carrinho com a nova lista
				session.setAttribute("carrinho", carrinho);
				
				// para verificação do produto
				break;
			}
		}
		return "";
	}
	
	@ResponseBody
	@RequestMapping(value = "/carrinho/exclui", method = RequestMethod.POST)
	public String excluir(Produto produto, HttpSession session) {
		// Pega carrinho da sessão e salva em variavel.
		@SuppressWarnings("unchecked")
		ArrayList<CarrinhoCompra> carrinho = (ArrayList<CarrinhoCompra>) session.getAttribute("carrinho");
		
		// Para cada CarrinhoCompra cc do carrinho da sessão:
		for(CarrinhoCompra cc : carrinho){
			// Se o SKU do produto é igual ao SKU  do produto do CarrinhoCompra cc
			if(cc.getProduto().getSku().equals(produto.getSku())){
				// salva o index do CarrinhoCompra da lista carrinho
				Integer index = carrinho.indexOf(cc);
				
				// Remove o produto da lista carrinho
				carrinho.remove(carrinho.get(index));
				
				// Seta o atributo carrinho com a nova lista
				session.setAttribute("carrinho", carrinho);
				
				// para verificação do produto
				break;
			}
		}
		return "";
	}
	
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value = "/carrinho/lista", method = RequestMethod.GET)
	public List<CarrinhoCompra> alistaCarrinho(HttpSession session) {
		return (ArrayList<CarrinhoCompra>) session.getAttribute("carrinho");
	}
}

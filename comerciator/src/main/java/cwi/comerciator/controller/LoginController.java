package cwi.comerciator.controller;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cwi.comerciator.dao.LoginDao;
import cwi.comerciator.model.Login;

@Controller
public class LoginController {
	
	@Inject
	private LoginDao loginDao;
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(Model model, HttpSession session) {
		if(session.getAttribute("login") != null){
			Login login = (Login) session.getAttribute("login");
			model.addAttribute("usuario", login.getNome());
			return "usuario.html";
		}else{
			model.addAttribute("mensagem", "");
			return "login.html";
		}
	}
	
	@RequestMapping(value = "/login/salvar", method = RequestMethod.POST)
	public String salvar(Login login, Model model, HttpSession session) {
		if(loginDao.emailExiste(login)){
			model.addAttribute("mensagem", "Email já cadastrado");
			return "redirect:/login";
		}else{
			login = loginDao.salvar(login);
			session.setAttribute("login", login);
			
			if(session.getAttribute("compraFinalizada") == "1") {
				return "redirect:/pagamento/dados";
			}else{
				return "redirect:/";
			}
		}
	}
	
	@RequestMapping(value = "/login/autentica", method = RequestMethod.POST)
	public String autencica(Login login, Model model, HttpSession session) {
		if(loginDao.validaDados(login)) {
			login = loginDao.buscaLoginPorDados(login);
			session.setAttribute("login", login);
			
			if(session.getAttribute("compraFinalizada") == "1") {
				return "redirect:/pagamento/dados";
			}else{
				loginDao.salvar(login);
				return "redirect:/";
			}
		}else{
			model.addAttribute("mensagem", "Usuario ou senha incorreto");
			return "login.html";
		}
	}
}

package cwi.comerciator.controller;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cwi.comerciator.dao.ProdutoDao;
import cwi.comerciator.model.Paginacao;
import cwi.comerciator.model.Produto;
import cwi.comerciator.model.Tag;

@Controller
public class ProdutoController {
	
	@Inject
	private ProdutoDao produtoDao;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String abreIndex(Model model){
		return "index.html";
	}
	
	@ResponseBody
	@RequestMapping(value="/produtos/buscaDescontos", method = RequestMethod.GET)
		public List<Produto> buscaUltimosProdutos(Model model){
			return produtoDao.buscaProdutosComMaioresDescontos();
	}
	
	@RequestMapping(value = "/produtos", method = RequestMethod.GET)
	public String abreProdutos(Model model){
		return "redirect:/produtos/pesquisa?busca=&pagina=1";
	}
	
	@RequestMapping(value = "/produto/detalhes", method = RequestMethod.GET)
	public String abreDetalhesProdutos(@RequestParam("id") int id, Model model){
		Produto produto = produtoDao.buscaProdutoPorId(id);
		
		Integer idProduto = produto.getIdProduto();
		String sku = produto.getSku();
		String nome = produto.getNome();
		String descricao = produto.getDescricao();
		String imagem = produto.getImagem();
		
		List<Tag> tags = produtoDao.tagsDoProduto(id);
		
		double preco = produto.getPreco();
		double desconto = produto.getDesconto();
		double precoTotal = preco - ((preco/100)*desconto);
		
		model.addAttribute("idProduto", idProduto);
		model.addAttribute("sku", sku);
		model.addAttribute("nome", nome);
		model.addAttribute("descricao", descricao);
		model.addAttribute("imagem", imagem);
		model.addAttribute("preco", preco);
		model.addAttribute("desconto", desconto);
		
		model.addAttribute("precoTotal", precoTotal);
		
		model.addAttribute("tags", tags);
		
		return "produto-detalhes.html";
	}
	
	@RequestMapping(value="/produtos/pesquisa", method = RequestMethod.GET)
	 public String abrePesquisas(@RequestParam("busca") String busca, @RequestParam("pagina") Integer pagina,Model model ){
		
		List<Produto> produtos = produtoDao.buscaProdutosPorNomeOuDescricao(busca, pagina);
		model.addAttribute("produtos", produtos);
		
		long totalDeProdutos = produtoDao.getTotalProdutos(busca);
		
		List<Paginacao> listaTotal = geraPaginacao(totalDeProdutos, pagina);
		
		model.addAttribute("numeroDePaginas",listaTotal);
		model.addAttribute("busca", busca);
		
		if(produtos.size() < 1){
			model.addAttribute("mensagem", "Nenhum produto encontrado!");
		}else{
			model.addAttribute("mensagem", null);
		}
		return "produtos-pesquisa.html";
	}
	

	@ResponseBody
	@RequestMapping(value="/produtos/buscaRelacionados", method = RequestMethod.GET)
	public List<Produto> buscaProdutosRelacionados(@RequestParam("idProduto") int idProduto, Model model){
		return produtoDao.buscaProdutosRelacionados(idProduto);
	}
	
	@RequestMapping(value="/produtos/pesquisaTag", method = RequestMethod.GET)
	 public String buscaTag(@RequestParam("busca") String busca, @RequestParam("pagina") Integer pagina,Model model ){
		
		List<Produto> produtos = produtoDao.buscaPorTag(busca, pagina);
		model.addAttribute("produtos", produtos);
		
		long totalDeProdutos = produtoDao.getTotalProdutosPorTag(busca);
		
		List<Paginacao> listaTotal = geraPaginacao(totalDeProdutos, pagina);

		model.addAttribute("numeroDePaginas",listaTotal);
		model.addAttribute("busca", busca);
		
		if(produtos.size() < 1){
			model.addAttribute("mensagem", "Nenhum produto encontrado!");
		}else{
			model.addAttribute("mensagem", null);
		}
		return "produtos-pesquisa.html";
	}
	
	/**
	 * Gera lista de 3 elementos de Paginação.
	 * @param totalDeProdutos Total de Produto selecionado
	 * @param pagina Pagina atual.
	 * @return List<Paginacao> Lista de paginação.
	 */
	public List<Paginacao> geraPaginacao(long totalDeProdutos, Integer  pagina) {

		List <Paginacao> listaTotal = new ArrayList<>();
		
		int numeroPaginas = (int) Math.ceil( totalDeProdutos / 9.0);
		
		int primeiraPagina = 0;
		int ultimaPagina = 0;
		
		boolean adicionaPaginacao = true;
		
		if(numeroPaginas == 1){
			adicionaPaginacao = false;
		}else if(pagina == 1){
			primeiraPagina = 1;
			ultimaPagina = numeroPaginas > 3 ? 3 : numeroPaginas;
		}else if(pagina + 1 > numeroPaginas) {
			primeiraPagina = pagina - 2 == 0 ? 1 : pagina - 2;
			ultimaPagina = pagina;
		}else if(pagina + 1 <= numeroPaginas && pagina -1 >= 1) {
			primeiraPagina = pagina - 1;
			ultimaPagina = pagina + 1;
		}
		
		if(adicionaPaginacao) {
			for(int i = primeiraPagina; i <= ultimaPagina; i++){
				listaTotal.add(new Paginacao(i, i == pagina ? true : false));
			}
		}
		
		return listaTotal;
		
	}
	
}

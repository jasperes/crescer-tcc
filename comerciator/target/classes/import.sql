INSERT INTO Instrutor (idInstrutor, nome) VALUES (INSTRUTOR_SEQ.NEXTVAL, 'Marlon Bernardes');
INSERT INTO Disciplina (idDisciplina, dificuldade, nome, idInstrutor) values (DISCIPLINA_SEQ.NEXTVAL, 9, 'JDBC', INSTRUTOR_SEQ.CURRVAL);
INSERT INTO Disciplina (idDisciplina, dificuldade, nome, idInstrutor) values (DISCIPLINA_SEQ.NEXTVAL, 7, 'Spring', INSTRUTOR_SEQ.CURRVAL);
INSERT INTO Disciplina (idDisciplina, dificuldade, nome, idInstrutor) values (DISCIPLINA_SEQ.NEXTVAL, 9, 'Java Web', INSTRUTOR_SEQ.CURRVAL);

INSERT INTO Instrutor (idInstrutor, nome) VALUES (INSTRUTOR_SEQ.NEXTVAL, 'André Nunes');
INSERT INTO Disciplina (idDisciplina, dificuldade, nome, idInstrutor) values (DISCIPLINA_SEQ.NEXTVAL, 9, 'SQL', INSTRUTOR_SEQ.CURRVAL);
INSERT INTO Disciplina (idDisciplina, dificuldade, nome, idInstrutor) values (DISCIPLINA_SEQ.NEXTVAL, 7, 'DDL', INSTRUTOR_SEQ.CURRVAL);
INSERT INTO Disciplina (idDisciplina, dificuldade, nome, idInstrutor) values (DISCIPLINA_SEQ.NEXTVAL, 8, 'Oracle', INSTRUTOR_SEQ.CURRVAL);

INSERT INTO Instrutor (idInstrutor, nome) VALUES (INSTRUTOR_SEQ.NEXTVAL, 'Bernardo Rezende');
INSERT INTO Disciplina (idDisciplina, dificuldade, nome, idInstrutor) values (DISCIPLINA_SEQ.NEXTVAL, 8, 'jQuery', INSTRUTOR_SEQ.CURRVAL);
INSERT INTO Disciplina (idDisciplina, dificuldade, nome, idInstrutor) values (DISCIPLINA_SEQ.NEXTVAL, 3, 'HTML', INSTRUTOR_SEQ.CURRVAL);
INSERT INTO Disciplina (idDisciplina, dificuldade, nome, idInstrutor) values (DISCIPLINA_SEQ.NEXTVAL, 5, 'CSS', INSTRUTOR_SEQ.CURRVAL);

-- Produtos que serão adicionados ao executar o servidor.
insert into produto (idProduto, sku, desconto, descricao, imagem, nome, preco) values (produto_seq.nextval, 'A', 0, 'Tamanho M', 'http://3.bp.blogspot.com/-JgLFcVS9lZo/UqhDWq1CTxI/AAAAAAAABrg/QSd3D-3YuGE/s1600/529.jpg', 'Camiseta Hocks', 80);
insert into produto (idProduto, sku, desconto, descricao, imagem, nome, preco) values (produto_seq.nextval, 'B', 0, 'Tamanho P', 'http://www.shopmasp.com.br/Imagens/produtos/35/72535/72535_Ampliada.jpg', 'Camiseta Adidas', 70);
insert into produto (idProduto, sku, desconto, descricao, imagem, nome, preco) values (produto_seq.nextval, 'C', 0, 'Tamanho G', 'http://www.gremiomania.com.br/upload/referenceAttribute/1398619604_camisa_oficial14_tric_C9000M_frente.jpg', 'Camiseta Gremio', 200);
insert into produto (idProduto, sku, desconto, descricao, imagem, nome, preco) values (produto_seq.nextval, 'D', 50, 'Tamanho G', 'http://wp.clicrbs.com.br/blogdobola/files/2013/12/camisavermelha.jpg', 'Camiseta Internacional', 200);
insert into produto (idProduto, sku, desconto, descricao, imagem, nome, preco) values (produto_seq.nextval, 'E', 25, 'Tamanho G', 'https://www.muralshoes.com.br/imagem/index/3015686/G/dudalina_azul_escuro.jpg', 'Camisa Dudalina', 180);
insert into produto (idProduto, sku, desconto, descricao, imagem, nome, preco) values (produto_seq.nextval, 'E', 25, 'Tamanho M', 'http://images.centauro.com.br/180x180/82708901.jpg', 'Camisa Polo', 110);
insert into produto (idProduto, sku, desconto, descricao, imagem, nome, preco) values (produto_seq.nextval, 'E', 25, 'Tamanho P', 'http://images.centauro.com.br/180x180/82717483.jpg', 'Camisa Polo', 125);
insert into produto (idProduto, sku, desconto, descricao, imagem, nome, preco) values (produto_seq.nextval, 'E', 25, 'Tamanho M', 'http://images.centauro.com.br/180x180/82719703.jpg', 'Camisa Polo', 80);
insert into produto (idProduto, sku, desconto, descricao, imagem, nome, preco) values (produto_seq.nextval, 'E', 25, 'Tamanho G', 'http://images.centauro.com.br/180x180/82451302.jpg', 'Camisa Polo', 80);
insert into produto (idProduto, sku, desconto, descricao, imagem, nome, preco) values (produto_seq.nextval, 'E', 25, 'Tamanho G', 'http://images.centauro.com.br/180x180/83396751.jpg', 'Camisa Polo', 110);
insert into produto (idProduto, sku, desconto, descricao, imagem, nome, preco) values (produto_seq.nextval, 'E', 25, 'Tamanho G', 'http://images.centauro.com.br/180x180/8082412V.jpg', 'Camiseta Flamengo', 190);
insert into produto (idProduto, sku, desconto, descricao, imagem, nome, preco) values (produto_seq.nextval, 'E', 25, 'Tamanho G', 'http://images.centauro.com.br/180x180/804575WY.jpg', 'Camiseta Corinthias', 150); 
insert into produto (idProduto, sku, desconto, descricao, imagem, nome, preco) values (produto_seq.nextval, 'F', 70, 'Tamanho 44', 'http://batecabeca.com.br/wp-content/uploads/imgext/foto-2012-09-26-23-56-20--550185698-Batecabeca.jpg', 'Calça Nike', 100);
insert into produto (idProduto, sku, desconto, descricao, imagem, nome, preco) values (produto_seq.nextval, 'E', 25, 'Tamanho M', 'http://images.centauro.com.br/180x180/82455604.jpg', 'Calção Adidas', 90);
insert into produto (idProduto, sku, desconto, descricao, imagem, nome, preco) values (produto_seq.nextval, 'E', 25, 'Tamanho 41', 'http://images.centauro.com.br/180x180/81097652.jpg', 'Tenis Adidas', 180);
insert into produto (idProduto, sku, desconto, descricao, imagem, nome, preco) values (produto_seq.nextval, 'E', 25, 'Laranja e Azul', 'http://images.centauro.com.br/180x180/816970CR.jpg', 'Bola Adidas', 75);
insert into produto (idProduto, sku, desconto, descricao, imagem, nome, preco) values (produto_seq.nextval, 'E', 25, 'Azul e Branco', 'http://images.centauro.com.br/180x180/77483411.jpg', 'Bola Nike', 80);
insert into produto (idProduto, sku, desconto, descricao, imagem, nome, preco) values (produto_seq.nextval, 'E', 25, 'Tamanho 41', 'https://www.muralshoes.com.br/imagem/index/2981133/M/preto_laranja.jpg', 'Tenis Nike', 320);
insert into produto (idProduto, sku, desconto, descricao, imagem, nome, preco) values (produto_seq.nextval, 'E', 25, 'Tamanho 39', 'https://www.muralshoes.com.br/imagem/index/2981116/M/cinza_verde.jpg', 'Tenis Nike', 230);
insert into produto (idProduto, sku, desconto, descricao, imagem, nome, preco) values (produto_seq.nextval, 'E', 25, 'Tamanho 40', 'http://images.centauro.com.br/180x180/8063120J.jpg', 'Tenis Asics', 170);
insert into produto (idProduto, sku, desconto, descricao, imagem, nome, preco) values (produto_seq.nextval, 'E', 25, 'Tamanho 42', 'http://images.centauro.com.br/180x180/823176XQ.jpg', 'Tenis Asics', 250);
insert into produto (idProduto, sku, desconto, descricao, imagem, nome, preco) values (produto_seq.nextval, 'E', 25, 'Tamanho 40', 'http://images.centauro.com.br/180x180/8230442W.jpg', 'Tenis Adidas', 170); --22

insert into produto (idProduto, sku, desconto, descricao, imagem, nome, preco) values (produto_seq.nextval, 'E', 25, 'Tamanho 56', 'http://images.centauro.com.br/180x180/81701304.jpg', 'Boné', 135);
insert into produto (idProduto, sku, desconto, descricao, imagem, nome, preco) values (produto_seq.nextval, 'E', 25, 'Tamanho 58', 'http://images.centauro.com.br/180x180/82181604.jpg', 'Boné', 120);
insert into produto (idProduto, sku, desconto, descricao, imagem, nome, preco) values (produto_seq.nextval, 'E', 25, 'Tamanho 58', 'https://www.muralshoes.com.br/imagem/index/4396091/M/11.jpg', 'Boné', 180);
insert into produto (idProduto, sku, desconto, descricao, imagem, nome, preco) values (produto_seq.nextval, 'E', 25, 'Tamanho 57', 'https://www.muralshoes.com.br/imagem/index/4396031/M/4.jpg', 'Boné', 165);
insert into produto (idProduto, sku, desconto, descricao, imagem, nome, preco) values (produto_seq.nextval, 'E', 25, 'Tamanho 46', 'http://img.kstatic.com.br/p/Kanui-Clothing-26-Co.-CalC3A7a-Sarja-Kanui-Clothing-26-Co.-Milan-0715-847821-1-catalog.jpg', 'Calça Jeans', 165);
insert into produto (idProduto, sku, desconto, descricao, imagem, nome, preco) values (produto_seq.nextval, 'E', 25, 'Tamanho 44', 'http://img.kstatic.com.br/p/Kanui-Clothing-26-Co.-CalC3A7a-Jeans-Kanui-Clothing-26-Co.-Destroyer-0339-547821-6-catalog.jpg', 'Calça Jeans', 165);
insert into produto (idProduto, sku, desconto, descricao, imagem, nome, preco) values (produto_seq.nextval, 'E', 25, 'Tamanho 42', 'http://img.kstatic.com.br/p/Kanui-Clothing-26-Co.-CalC3A7a-Sarja-Kanui-Clothing-26-Co.-Milan-4924-057821-1-catalog.jpg', 'Calça Jeans', 165);
insert into produto (idProduto, sku, desconto, descricao, imagem, nome, preco) values (produto_seq.nextval, 'E', 15, 'Tamanho M', 'https://www.myshirtshack.co.uk/image/cache/data/Breaking%20Bad/Jesse%20Bitch/breaking-bad-obey-bitch-funny-white-t-shirts_zpsdd606884-600x600.jpg', 'Camiseta Jesse Pinkman', 80);
-- Produtos DESTAQUES que serão adicionados ao executar o servidor.
insert into destaque (idDestaque, conteudo, imagem, link, titulo) values (destaque_seq.nextval, '"Lorem ipsum dolor sit amet."', 'http://3.bp.blogspot.com/-JgLFcVS9lZo/UqhDWq1CTxI/AAAAAAAABrg/QSd3D-3YuGE/s1600/529.jpg','/produto/detalhes?id=1','Camiseta Hocks' );	
insert into destaque (idDestaque, conteudo, imagem, link, titulo) values (destaque_seq.nextval, '"Lorem ipsum dolor sit amet."', 'https://www.myshirtshack.co.uk/image/cache/data/Breaking%20Bad/Jesse%20Bitch/breaking-bad-obey-bitch-funny-white-t-shirts_zpsdd606884-600x600.jpg','/produto/detalhes?id=21','Camiseta Jesse Pinkman' );	
insert into destaque (idDestaque, conteudo, imagem, link, titulo) values (destaque_seq.nextval, '"Lorem ipsum dolor sit amet."', 'https://www.muralshoes.com.br/imagem/index/3015686/G/dudalina_azul_escuro.jpg','/produto/detalhes?id=5','Camisa Dudalina' );	

insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'marca', 'Hocks');
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'marca', 'nike');
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'marca', 'adidas');
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'marca', 'puma');
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'marca', 'under armour');
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'marca', 'asics');
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'marca', 'dalponte');
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'marca', 'topper');
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'marca', 'umbro');
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'marca', 'everlast');
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'marca', 'polo');
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'marca', 'dudalina'); --12

insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'esportivo', 'camiseta'); --13
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'esportivo', 'camisa');
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'esportivo', 'calcao');
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'esportivo', 'tenis');
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'esportivo', 'bola');
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'esportivo', 'luvas'); --18

insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'masculino', 'camiseta'); --19
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'masculino', 'calca');
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'masculino', 'camisa');
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'masculino', 'tenis');
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'masculino', 'calcao');
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'masculino', 'bone');
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'masculino', 'agasalho');
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'masculino', 'blusao');
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'masculino', 'casaco');
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'masculino', 'moletom');
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'masculino', 'chinelo');
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'masculino', 'sapato'); --30

insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'feminino', 'camiseta'); --31
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'feminino', 'calca');
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'feminino', 'camisa');
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'feminino', 'tenis');
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'feminino', 'bone');
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'feminino', 'agasalho');
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'feminino', 'blusa');
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'feminino', 'blusao');
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'feminino', 'casaco');
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'feminino', 'moletom');
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'feminino', 'chinelo');
insert into tag(idTag, classificacao, descricao) values (tag_seq.nextval, 'feminino', 'sapato'); --42

insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 1, 1);
insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 19, 1);

insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 3, 2);
insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 13, 2);

insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 2, 3);
insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 13, 3);

insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 2, 4);
insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 13, 4);

insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 12, 5);
insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 21, 5);

insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 4, 6);
insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 21, 6);

insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 11, 7);
insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 21, 7);

insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 11, 8);
insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 21, 8);

insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 3, 9);
insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 21, 9);

insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 2, 10);
insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 21, 10);

insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 3, 11);
insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 13, 11);

insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 2, 12);
insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 13, 12);

insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 2, 13);
insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 20, 13);

insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 3, 14);
insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 15, 14);

insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 3, 15);
insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 16, 15);

insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 3, 16);
insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 17, 16);

insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 2, 17);
insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 17, 17);

insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 2, 18);
insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 22, 18);

insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 2, 19);
insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 22, 19);

insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 6, 20);
insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 34, 20);

insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 6, 21);
insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 34, 21);

insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 3, 22);
insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 34, 22);

insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 2, 23);
insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 24, 23);

insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 5, 24);
insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 24, 24);

insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 5, 25);
insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 24, 25);

insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 5, 26);
insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 24, 26);

insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 5, 27);
insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 20, 27);

insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 5, 28);
insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 20, 28);

insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 5, 29);
insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 20, 29);

insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 5, 30);
insert into tagproduto(idTagProduto, idTag, idProduto) values (tagproduto_seq.nextval, 19, 30);